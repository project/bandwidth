Bandwidth module
----------------

This module allows users to specify a preferred bandwidth for viewing videos. It is designed to be used with other modules and is useless alone.

This module is developed by Koumbit and sponsored by Isuma.tv.

Features include :
- Block with links to choose the preferred bandwidth
- Option in the user profile to change the bandwidth
- Name of the bandwidth is included at the beginning of all intern URLs (http://www.example.com/lo/page for low bandwidth setting). If a user change the name of the bandwidth in the url, his bandwidth preferences are automatically updated
- Support for anonymous users

Requirements :
- Drupal 6
- Url alter module (http://drupal.org/project/url_alter) is recommended if you are using other modules that change internal urls.